"""
defaults for mqtt watchdog
"""
# pylint: disable=line-too-long

DEFAULT_CONFIG={
    "watchdog_topic": "watchdog",          # The root topic to publish status messages under
                                           # Summary messages will be published on
                                           # "<watchdog_topic>/<watchdog_topic>"
                                           # eg.  "watchdog/watchdog"
    "mqtt_broker":    "mqtt.example.org",  # mqtt broker address.
    "mqtt_port":      8883,                # mqtt broker port - must use TLS.
    "mqtt_user":      "username",          # Username for mqtt.
    "mqtt_password":  "secret_password",   # Password for mqtt.
    "mqtt_cacert":    "/etc/ssl/certs/ISRG_Root_X1.pem",  # Usually Let's Encrypt root certificate.
    "mqtt_tls":       False,               # Whether or not to use TLS, default False.
    "mqtt_anonymous": False,               # Set true to use no username/password.
    # NOTE: all times are in seconds
    "history_check_period": 5,             # How often to check topics for failure.
    "max_age":             10,             # Max age of a topic before it is considered to have failed.
    "summary_period":     300,             # How often to republish summary of status, if nothing has changed.
    "summary_delay":       30,             # Max time after a state change before summary is published.
    "subscribe":          "#",             # Base topic to subscribe to - "#" means everything.
    "exclude_topics": [ "^test/", "^time/" ], # list of regexes for topics to ignore
    "healthcheck_period": 900,              # how often to ping healthchecks server
    "healthcheck_url":    "",               # healthchecks.io URL
}
