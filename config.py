"""
config reader
"""
# pylint: disable=fixme,import-error

#import collections
import os
#import re
#import ssl
import sys
#import time

import yaml
from default_config import DEFAULT_CONFIG
#import paho.mqtt.client as mqtt

#--------------------------------------------------------------------------
def read_config(configfile):
    """
    read config
    """

    try:
        with open(configfile) as stream:
            config = yaml.safe_load(stream)
    except IOError:
        print("FATAL: Could not read '%s'" % configfile)
        sys.exit(1)

    for key,val in DEFAULT_CONFIG.items():
        if key not in config:
            print("Config key '%s' set to default value '%s'" % (key,val))
            config[key] = val

    for key in sorted(config):
        if key == "mqtt_password":
            print("Config: '%s' = <REDACTED>" % (key))
        else:
            print("Config: '%s' = %s" % (key, config[key]))

    return config

#--------------------------------------------------------------------------
if __name__ == "__main__":

    if __name__ == '__main__':
        # If the module is executed as a script __name__ will be '__main__'
        # and sys.argv[0] will be the full path of the module.
        SCRIPTPATH = os.path.split(sys.argv[0])[0]
    else:
        # Else the module was imported and it has a __file__ attribute
        # that will be the full path of the module.
        SCRIPTPATH = os.path.split(__file__)[0]

    _ = read_config(
        os.path.join(SCRIPTPATH, 'config.yaml')
        )
