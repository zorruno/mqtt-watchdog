#! /bin/sh

cd "$(dirname "$0")" || {
   echo "ERROR: could not CD to script dir"
   exit 1
}

./venv/bin/python ./mqtt-watchdog.py

