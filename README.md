# mqtt-watchdog

## About

This python script monitors a configurable set of topics from an mqtt broker and alerts if any of the topics cease to be received.  Only topics which have been published at least once will be monitored.

NOTE: This is work-in-progress, but it seems to be working for my use.  Connecting without TLS, or without username/password, are currently untested.

### Operation

A pair of lists are maintained, holding good and overdue topics.  Each list contains the topics and the last-received times.  Both lists are empty until a topic has been received.  Topics are initially recorded in the good list, and move to overdue as/when they are found to be late, and back again if they resume arriving.

The watchdog has a base topic under which it publishes status information (eg. `watchdog`).  If a subscribed topic (eg. `sensor/water/bathroom`) is found to be overdue, a JSON message will be published on the corresponding topic under the base topic (eg. `watchdog/sensor/water/bathroom`).  If the topic later starts arriving, another JSON message will be published (eg. on `watchdog/sensor/water/bathroom`) with the new state.

A status summary message containing counts for good and overdue messages is sent periodically.  If the watchdog base topic is `watchdog_test`, this summary will be sent on `watchdog_test/watchdog_test`.  This topic is chosen because the watchdog will never subscribe to itself so will never be publishing a failure message for itself.  The summary message is normmally sent on a regular schedule, but that schedule is brought forward upon a topic being found overdue - eg. a default setup might have the summary published every 5 minutes, but whenever a topic is found overdue, the summary is brought forward to happen within 30s of that.  If new topics continually failed, the summary would be sent every 30s, but no more often.

Content of received messages is ignored.

## Topic configuration overview

There are 2 layers of topic selection.

- The first level is the mqtt subscription - only topics matching this will be considered.
- The second level is a list of regexes matching topics to exclude.  Note: the watchdog base topic for publishing will automatically be added to the exclude list so that it doesn't attempt to monitor itself.

## Example operation

For the following configuration:

```
history_check_period:  30
max_age:              120
summary_period:       300
summary_delay:         30
watchdog_topic: watchdog
subscribe: "#"
exclude_topics:
   - '.*/LWT$'
   - '^tasmota/discovery/'
```

- Event: script started up - no history.
- Event: topic `tasmota/discovery/1337DEADBEEF/config` arrives.
- Action: no action because this topic is excluded.

- Event: topic `sensor/temperature/kitchen` arrives.
- Action: `sensor/temperature/kitchen` added to "good" list with arrival timestamp.
- Event: 120 seconds pass by.
- Action: `sensor/temperature/kitchen` and its timestamp moved to "overdue" list.
- Action: JSON overdue message sent on `watchdog/sensor/temperature/kitchen`
- Action: Timing for next status message brought forward to be <=30s from now.
- Event: up to 30 seconds pass by.
- Action: JSON status message sent on `watchdog/watchdog`, listing counts of 0 good and 1 bad.

- Event: topic `sensor/temperature/kitchen` arrives again.
- Action: `sensor/temperature/kitchen` added to "good" list with arrival timestamp, and removed from "overdue" list.
- Action: Timing for next status message brought forward to be <=30s from now.






## Installation into a virtualenv

Clone the repository, then `cd` into that folder.

Create the virtualenv like this `python3 -m venv venv`  (note: if you move/rename the virtualenv after creation, it will need fixing)

Install the requirements like this `./venv/bin/pip install -r requirements.txt`

## Configuration & run

Copy `config.yaml.sample` to `config.yaml` and edit to suit your needs.  Comments/help are in `default_config.py`

Edit `go.sh` to suit needs, then run it.

## Miscellaneous

Tested on Debian 10.11 with Python 3.7.3

Licensed under GPL v3

### Design Considerations

This script has been designed for home use - eg. perhaps with a broker running on a low-end Raspberry Pi, and monitoring a few hundred topics.  It may well work for larger setups, but has not been tested for that, and some parameters may need adjusting.

All timing is managed with integer unix epoch times, so has resolution of 1s.  Accuracy is intended to be good enough to monitor topics which are each typically published roughly once per minute, or less frequent.

It is assumed that notice of failure is required within a reasonable timeframe but, because there is likely no 24/7 Ops Team to respond immediately, some leeway is suitable.

No more than 1 overdue message will be published per second, even if multiple topics fail at once.  This is intended to reduce load on the script and broker if a significant piece of infrastructure fails which causes multiple topics to fail at once.  The status summary message is separately rate-limited from the overdue messages.

### Future improvements

- option to check received message size?  probably useless.
- dynamic overdue timing?
   - for each topic, record last 10 message timestamps, and alert if next message is later than 5 average intervals (to allow some dropped messages without alarm).
- support multiple subscriptions/exclusions/timing rule sets within a single instance of the script?
   - the primary benefit for this is to have a single summary status message for all topics.
   - at present, the only option is to run separate instances, and publish on separate summary topics.
      - if there is no need for summaries, there would be no issue with having different instance subscribing to different topics and publishing beneath the same base topic.
- serve a webpage listing good/overdue topics.
   - include option to "forget" an overdue topic so it is no longer included in the summary counts

### History

- 2021-10-13 - support multiple subscription topics
- 2021-10-09 - add healthchecks.io support
- 2021-09-25 - begin

