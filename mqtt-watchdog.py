"""
mqtt-watchdog.
Subscribe to topics, collect active ones into a list, and send an alert if any of them
stop publishing.
"""
# pylint: disable=fixme,import-error

import collections
import os
import re
import ssl
import sys
import time

import requests
import paho.mqtt.client as mqtt

from config import read_config

IN_Q_MAX  = 1000
OUT_Q_MAX = 1000

# enumerate good/bad
GOOD_STATE = 1
BAD_STATE  = 0

GOOD_HEALTHCHECK = 1
BAD_HEALTHCHECK  = 0


#--------------------------------------------------------------------------
def on_connect(mqttc, userdata, _flags, resultcode):
    """
    The callback for when the client receives a CONNACK response from the server.
    """
    print("Connected with result code "+str(resultcode))

    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    subs = userdata["subscribe"]
    if isinstance(subs, str):
        # if we were given just one subscription, make an array of it
        subs = [subs]
    # convert array to form of:
    #    [("topic",QoS1), ("topic2",QoS2), ...]
    # where the QoS is always 0
    subs_qos = [(topic,0) for topic in subs]
    print("connecting and subscribing to: %s" % str(subs_qos))
    mqttc.subscribe(subs_qos)

#--------------------------------------------------------------------------
def on_message(_mqttc, userdata, msg):
    """
    The callback for when a PUBLISH message is received from the server.
    """
    #if msg.topic.startswith(WD_TOPIC+"/"):
    #    print("IGNORE SELF: " + msg.topic)
    if userdata["exclude_rgx"].match(msg.topic):
        print("EXCLUDE: " + msg.topic)
    else:
        if IN_Q_MAX == len(userdata["in_q"]):
            # FIXME: better handling - should publish error
            print("ERROR: in_q overflow")
        now = int(time.time())
        userdata["in_q"].append([now, msg.topic])

#--------------------------------------------------------------------------
def process_input(in_q, out_q, good_timestamps, bad_timestamps):
    """
    Deal with anything in the input buffer.
    """
    #if len(userdata["in_q"]) > userdata["peak_in_q"]:
    #     userdata["peak_in_q"] = len(userdata["in_q"])

    while in_q:
        timestamp, topic = in_q.popleft()
        good_timestamps[topic] = timestamp  # NOTE: mqtt topics are case-sensitive
        if topic in bad_timestamps:
            last_time = bad_timestamps[topic]
            del bad_timestamps[topic]
            print("BAD-->GOOD: " + topic)
            out_q.append({
                "topic":     topic,
                "timestamp": timestamp,
                "gap":       timestamp-last_time,
                "state":     GOOD_STATE,
                })
            # TODO: publish something
        else:
            print("GOOD: " + topic)

#--------------------------------------------------------------------------
def process_history(out_q, good_timestamps, bad_timestamps, max_age):
    """
    Examine all the history of messages and see if any have not arrived recently
    """
    print("      checking history")
    timestamp_bad = int(time.time()) - max_age
    for topic in list(good_timestamps.keys()):
        topic_timestamp = good_timestamps[topic]
        if topic_timestamp < timestamp_bad:
            out_q.append({
                "topic":     topic,
                "timestamp": topic_timestamp,
                "gap":       0,
                "state":     BAD_STATE,
                })
            # no message for too long
            print("GOOD-->BAD: " + topic + "  since: " + str(int(time.time()) - topic_timestamp))
            # remember bad topic so we can notify transition when it comes good
            bad_timestamps[topic] = topic_timestamp
            # forget topic so we don't try to call it bad again on next process_history cycle
            del good_timestamps[topic]

#--------------------------------------------------------------------------
def process_publishing(out_q, mqttc, watchdog_base_topic):
    """
    Publish any queued messages
    """
    #print("      processing output queue")
    # only send one message at a time, to keep load on broker low
    if out_q:
        out = out_q.popleft()  # topic, timestamp, state
        #print (",".join(out.keys()))
        wd_topic = "%s/%s" % (watchdog_base_topic, out["topic"])
        msg_data = {
            "topic":     out["topic"],
            "timestamp": int(time.time()),
            "gap":       out["gap"],
            "state":     out["state"]
            }
        if GOOD_STATE == out["state"]:
            msg_template = '{ "topic":"%(topic)s", "timestamp":%(timestamp)i, "gap":%(gap)s, "state":%(state)i }'  # pylint: disable=line-too-long
        else:
            msg_template = '{ "topic":"%(topic)s", "timestamp":%(timestamp)i, "state":%(state)i }'  # pylint: disable=line-too-long
        wd_msg = msg_template % msg_data
        print("      sending: %s:  %s" % (wd_topic, wd_msg))
        mqttc.publish(wd_topic, wd_msg)

#--------------------------------------------------------------------------
def publish_summary(mqttc, watchdog_base_topic, good_count, bad_count):
    """
    Publish summary of status
    """
    wd_topic = "%s/%s" % (watchdog_base_topic, watchdog_base_topic)
    msg_data = {
        "timestamp": int(time.time()),
        "good":      good_count,
        "bad":       bad_count
        }
    msg_template = '{ "timestamp":"%(timestamp)i", "good":%(good)i, "bad":%(bad)i }'
    wd_msg = msg_template % msg_data
    print("SUMMARY: good=" + str(good_count)
          + " bad=" + str(bad_count)
          + " (topic=" + wd_topic + ")")
    mqttc.publish(wd_topic, wd_msg)

#--------------------------------------------------------------------------
def ping_healthcheck(url, state):
    """
    Ping healthchecks.io URL
    """
    healthcheck_url = url
    if state == BAD_STATE:
        healthcheck_url += "/fail"

    print("curling %s" % healthcheck_url)
    try:
        requests.get(healthcheck_url, timeout=10)
    except requests.RequestException as error:
        # Log ping failure here...
        print("ERROR: curl failed for %s - %s" % (healthcheck_url, error))

#--------------------------------------------------------------------------
#--------------------------------------------------------------------------
#--------------------------------------------------------------------------
def main():
    """
    Subscribe to topics, collect active ones into a list, and send an alert if any of them
    stop publishing.
    """

    if __name__ == '__main__':
        # If the module is executed as a script __name__ will be '__main__'
        # and sys.argv[0] will be the full path of the module.
        scriptpath = os.path.split(sys.argv[0])[0]
    else:
        # Else the module was imported and it has a __file__ attribute
        # that will be the full path of the module.
        scriptpath = os.path.split(__file__)[0]

    config = read_config(
        os.path.join(scriptpath, 'config.yaml')
        )

    good_timestamps = {}
    bad_timestamps  = {}

    # we publish on "WD_TOPIC/topic/of/failed/message", so exclude that too
    exclude_rgx = list(config["exclude_topics"])
    exclude_rgx.append("^" + config["watchdog_topic"] + "/")
    exclude_rgx = re.compile("(" + '|'.join(exclude_rgx) + ")")

    # My convention for a fifo is to append and popleft.  (so head is q[0])
    # in_q is only appended to in callback & popped in main loop.
    in_q =  collections.deque([], IN_Q_MAX)
    out_q = collections.deque([], OUT_Q_MAX)

    mqttc = mqtt.Client(
        userdata={
            "exclude_rgx": exclude_rgx,
            "in_q":        in_q,
            "subscribe":   config["subscribe"],
            #"peak_in_q":   0,
            }
        )

    if not config["mqtt_anonymous"]:
        mqttc.username_pw_set(
            username=config["mqtt_user"],
            password=config["mqtt_password"]
            )

    if config["mqtt_tls"]:
        mqttc.tls_set(
            config["mqtt_cacert"],
            tls_version=ssl.PROTOCOL_TLSv1_2
            )

    mqttc.on_connect = on_connect
    mqttc.on_message = on_message

    print('Connecting...')
    mqttc.connect_async(
        config["mqtt_broker"],
        config["mqtt_port"],
        60
        )
    mqttc.loop_start()

    next_summary_time  = int(time.time()) + config["summary_period"]
    next_history_check = int(time.time()) + config["history_check_period"]
    next_healthcheck   = int(time.time()) + config["healthcheck_period"]
    if "healthcheck_url" in config:
        if config["healthcheck_url"]:
            ping_healthcheck(config["healthcheck_url"], GOOD_HEALTHCHECK)

    print('Press Ctrl-C to exit...')
    try:
        while True:
            now = int(time.time())
            process_input(in_q, out_q, good_timestamps, bad_timestamps)
            if now >= next_history_check:
                # it is time to process history and look for things that have gone bad
                next_history_check += config["history_check_period"]
                process_history(out_q, good_timestamps, bad_timestamps, config["max_age"])
            if now >= next_summary_time:
                # it's time to publish periodic summary of status
                # FIXME: publish summary
                publish_summary(mqttc,
                                config["watchdog_topic"],
                                len(good_timestamps),
                                len(bad_timestamps)
                               )
                #print("peak input queue length = " + userdata["peak_in_q"])
                next_summary_time += config["summary_period"]
            elif ((next_summary_time - now) > config["summary_delay"]) and out_q:
                # bring forward the summary if it is distant, but we have changes
                print("bringing forward summary")
                next_summary_time = now + config["summary_delay"]
            if "healthcheck_url" in config and (now >= next_healthcheck):
                # TODO: use mqtt connected state in healthcheck?
                # remember how long since last connected, and ping fail if it is too long?
                ping_healthcheck(config["healthcheck_url"], GOOD_HEALTHCHECK)
                next_healthcheck += config["healthcheck_period"]

            process_publishing(out_q, mqttc, config["watchdog_topic"])
            time.sleep(1)  # FIXME: make configurable
    except KeyboardInterrupt:
        print()
        print('...Cancelled via Ctrl-C')

    print("Disconnecting")
    if "healthcheck_url" in config:
        if config["healthcheck_url"]:
            ping_healthcheck(config["healthcheck_url"], BAD_HEALTHCHECK)
    mqttc.loop_stop()

#--------------------------------------------------------------------------
if __name__ == "__main__":
    main()
