FROM python:3-alpine

RUN mkdir -p /app
RUN apk add --no-cache curl
RUN pip install --upgrade pip
RUN pip install requests paho-mqtt PyYAML
WORKDIR "/app"
RUN curl https://gitlab.com/api/v4/projects/30305594/repository/files/mqtt-watchdog.py/raw\?branch\=master > mqtt-watchdog.py
RUN curl https://gitlab.com/api/v4/projects/30305594/repository/files/config.py/raw\?branch\=master > config.py
RUN curl https://gitlab.com/api/v4/projects/30305594/repository/files/default_config.py/raw\?branch\=master > default_config.py
COPY data/config.yaml /app/config.yaml
CMD ["python", "/app/mqtt-watchdog.py"]

